# Cosmonarchy

Cosmonarchy, a comprehensive overhaul of Starcraft version 1.16.1, is the prototype of the definitive RTS experience. It is the most ambitious Starcraft mod to ever see play.

**AS OF MARCH 2024**, project co-developer Veeq7 has completed the latest and greatest Fraud Launcher! [Get it here!](https://fraudsclub.com/files/Fraud%20Launcher%202.zip)

Should you run into any issues, see [this page](https://www.fraudsclub.com/cosmonarchy-bw/sites/game-setup/).

## Project info

Please send any feedback to us through [our discord server](https://discordapp.com/invite/s5SKBmY)!

For prerelease builds, see [this repo](https://gitlab.com/Pr0nogo3/specialbus).

For more info on Cosmonarchy, including a project introduction and primer, visit the project's [website](http://fraudsclub.com/cosmonarchy-bw). You can also follow development progress by consulting our [Trello board](https://trello.com/b/ZuOmqT8i/cosmonarchy).

Source code repos for the plugins used in Cosmonarchy are available in [our tools group](https://gitlab.com/the-no-frauds-club/tools).

## Current caveats
**Occasional crashes** may occur. If you run into such behavior, please report the game state near the point of the crash, and any actions you may have taken just before the crash. This will help us resolve them as quickly as possible.

**AI performance** largely relies on how stable their build orders are. These are continuously improved from patch to patch. You can report suboptimal AI play by telling us their player name and early structures/decisions.

**Game performance** occasionally suffers while hundreds of AI-controlled units calculate pathing. This performance impact resolves when the paths are fully calculated. An improvement to this may be looked at in the future.

**Audiovisuals** for new units and custom abilities are lacking, and will continuously be added from patch to patch.