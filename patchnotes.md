# 22 February 2025
## Year 3, month 2, patch 2.1

This patch includes new units and features, balance adjustments, bugfixes, and audiovisual improvements!

Download [Fraud Launcher 2](https://fraudsclub.com/files/Fraud%20Launcher%202.zip) for easier setup and maintenance! It now includes automatic setup and updates for CrownLink, a networking alternative for Cosmonarchy!

## Next patch...
Our goals for the next patch (8 March 2025) include:
- Reworked [Savant](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/savant), [Siren](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/siren), and [Mind Tyrant](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/mind-tyrant)
- Deployment of the [Tencendur](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/tencendur), [Xenolith](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/xenolith), and [Zobriolisk](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/zobriolisk) (concepts will be adjusted!)
- More factional units for Golden Republic, Nydus Trinity, Starbound, and Imicren Tendril
- AI and stability improvements

## HOTFIX - 23 February 2025
- Fixed a common crash when unloading turreted units from transports
- Fixed a rare crash related to rescuable units in scenarios (r. & c. DF)

## Mapmaking
- Added crig action `announce_player_defeat`

## Advisors
- Added 'Golden Republic' advisor
	- **Caveat:** Won't automatically apply in Faction Wars, yet!

## Game Modes
- **Faction Wars:**
	- Enabled **Khalendric Knights** for Protoss
	- Faction selection now includes player name

## Bugfixes
- **Gameplay:**
	- Fixed garrisons being unable to unload units in legal positions (r. SmrtyCakes)
- **AI:**
	- Now properly defends with tier 1 units, regardless of whether it has the required structure or not
	- Now properly requests factional units when applicable

## Audiovisuals
- New corpses:
	- **Shaman**, **Goliath**, **Madrigal**, **Wraith** (c. Daedrid)
- New wireframes:
	- **Golem**, **Ocsal Cesiant** (c. jun3hong)
- New unit responses:
	- **Aspirant**, **Cedent**
	- **Sieve**, **Eskigant**, **Kazitrith Cesiant**, **Ocsal Cesiant** (c. jun3hong)
- Repaletted a few jungle tree doodads for use in other tilesets

## Terran
- [Ministry](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/ministry):
	- Build time 75 » 65
- [Treasury](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/treasury):
	- Build time 40 » 35
- [Lodestone](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/lodestone):
	- Build time 25 » 20
- [Sieve](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/sieve):
	- HP 400 » 500
- [Stockade](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/stockade):
	- Resource costs 125/0 » 150/0
	- Build time 50 » 45
- [Fulcrum](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/fulcrum):
	- Resource costs 175/50 » 200/50
	- Build time 50 » 45
- [Cohort](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/cohort):
	- **Servile Vigor:**
		- Now has a 1-second cooldown post-revive before a corpse can be consumed

## Protoss
- [Dracadin](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/dracadin):
	- HP 100 » 120
- [Atreus](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/atreus):
	- Resource costs 175/50 » 200/50
	- HP 160 » 200
	- Shields 60 » 80
- [Manifold](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/manifold):
	- Resource costs 75/25 » 50/25
	- Transport weight 2 » 1
- [Idol](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/idol):
	- Build time 28 » 25
- [Positron](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/positron):
	- Resource costs 150/50 » 125/50
	- **Plasma Missiles:**
		- Weapon cooldown 1 » 0.75
- [Accantor](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/accantor):
	- **Plasma Shell:**
		- Weapon range 8 » 9
		- Now travels to max range
- [Clarion](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/clarion):
	- Now trained at Ardent Authority, from Synthetic Synod
	- Build time 30 » 35
	- HP 60 » 80
	- Shields 60 » 40
- [Architect](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/architect):
	- Now trained at Unitary Union, from Ardent Authority
	- Weapon range 9 » 12
- [Analogue](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/analogue):
	- Now trained at Synthetic Synod, from Ardent Authority
	- Resource costs 100/200 » 200/200
	- HP 140 » 220
	- Shields 80 » 110
	- Armor 2 » 3
	- **Cylindrical Blaster:**
		- Weapon damage 2x6 » 2x10
		- Armor penetration 2 » 4
		- Weapon cooldown 1 » 0.83
	- **Empathy Core:**
		- Hits now drain 5 energy, from 10
- [Nexus](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/nexus):
	- Build time 75 » 65
- [Cedent](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/cedent):
	- HP 250 » 350
- [Gateway](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/gateway):
	- Resource costs 150/0 » 175/0
	- Build time 55 » 50
- [Ardent Authority](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/ardent-authority):
	- Resource costs 350/200 » 300/200
	- HP 750 » 700
	- Shields 500 » 450
- **NEW:** [Unar](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/unar):
	- Sniper automaton, trained from Unitary Union
- **NEW:** [Oscilliant](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/oscilliant):
	- Binding automaton, trained from Unitary Union
- **NEW:** [Vibrance](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/vibrance):
	- Disruptive automaton, trained from Unitary Union
- **NEW:** [Unitary Union](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/unitary-union):
	- Crowd-controlling robotic production; requires any tier 2
- **NEW:** [Ecclesiast (Khalendric)](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/ecclesiast-khalendric):
	- Khalendric Knights: Ecclesiast variant; ascends nearby allies
- **NEW:** [Ashguard](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/ashguard):
	- Khalendric Knights: Hierophant replacement; annexes victims
- **NEW:** [Renesair](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/renesair):
	- Order of the Sacred Saber: Vassal replacement; self-replicates
- **NEW:** [Extatus](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/extatus):
	- Order of the Sacred Saber: Aurora replacement; restores shields a la Matrix

## Zerg
- [Nathrokor](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/nathrokor):
	- Target acquisition range 3 » 5
- [Protathalor](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/protathalor):
	- **Pungent Spores:**
		- Now targets air & ground, from ground only
- [Hachirosk](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/hachirosk):
	- Build time 65 » 55
- [Larvosk Cesiant](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/larvosk-cesiant):
	- Build time 35 » 30
- [Eskigant](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/eskigant):
	- HP 350 » 450
- [Kazitrith Cesiant](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/kazitrith-cesiant):
	- Transport weight 6 » 10
- [Hydrith Den](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/hydrith-den):
	- Build time 50 » 55
- [Skibact Scab](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/skibact-scab):
	- Build time 40 » 45